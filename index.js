const express = require(`express`);
const fs = require(`fs`);

const app = express();

const isi = fs.readFileSync("./text.txt", "utf-8");
console.log(isi);

fs.writeFileSync("./text1.txt", "I love Decoding");
const isi2 = fs.readFileSync("./text1.txt", "utf-8");
console.log(isi2);

// const data = require(`./create.js`);
const car = require(`./car.json`);
// console.log(car);

app.get(`/`, (req, res) => {
  res.render(`index.ejs`);
});

app.get(`/car`, (req, res) => {
  res.render(`car.ejs`, {
    data: car,
  });
});
app.listen(3000);
