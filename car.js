const fs = require("fs");

function addcar(brand, model, type, seat, machine, production) {
  const dataBuffer = fs.readFileSync("./car.json", "utf-8");
  const data = JSON.parse(dataBuffer);
  const dataBaru = {
    brand,
    model,
    type,
    seat,
    machine,
    production,
  };
  data.push(dataBaru);
  console.log(data);
  fs.writeFile("./car.json", JSON.stringify(data), "utf-8", (err) => {
    if (err) throw err;
    console.log("Data Successfully Saved");
  });
}

addcar("Honda", "CR-V", "SUV", "5", "2.0L", "155PS", "2021");
